import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection

def createRectGrid(xlow,xhigh,ylow,yhigh,Nx,Ny):
    x = np.linspace(xlow, xhigh, Nx)
    y = np.linspace(ylow, yhigh, Ny)
    X, Y = np.meshgrid(x, y)
    points = np.vstack([X.flatten(), Y.flatten()]).transpose()
    cells = np.empty([(Nx-1)*(Ny-1), 4])
    k = 0
    for i in range((Nx*Ny)-Nx):
        if (i+1) % Nx != 0:
            cells[k, :] = [i, i+1, i+Nx, i+Nx+1]
            k += 1
    return points, cells


def createTriGrid(xlow,xhigh,ylow,yhigh,Nx,Ny):
    points, rect_cells = createRectGrid(xlow, xhigh, ylow, yhigh, Nx, Ny)
    cells = np.empty([2 * rect_cells.shape[0], 3])
    k = 0
    for i in range(len(rect_cells)):
        cells[k, :] = rect_cells[i][0:3]
        cells[k+1, :] = rect_cells[i][-1:0:-1]
        k += 2
    return points, cells


def showGrid(points,cells):
    plt.figure()
    fig, ax = plt.subplots()
    dimension = np.shape(cells)[1]
    polygon_collection = []
    if dimension == 3:
        plt.scatter(points[:, 0], points[:, 1])
        for i in range(np.shape(cells)[0]):
            polygon_collection.append(plt.Polygon(points[cells[i].astype(int)]))
        #plt.triplot(points[:, 0], points[:, 1], cells)
    elif dimension == 4:
        plt.scatter(points[:, 0], points[:, 1])
        for i in range(np.shape(cells)[0]):
            polygon_collection.append(plt.Polygon(points[cells[i].astype(int)][[0, 1, 3, 2]]))
    else:
        raise NotImplementedError
    p = PatchCollection(polygon_collection, alpha=0.4, edgecolors='red')
    ax.add_collection(p)
    plt.show()

def main():
    xlow = 0
    ylow = 0
    xhigh = 4
    yhigh = 2
    Nx = 20
    Ny = 6

    rect_points, rect_cells = createRectGrid(xlow, xhigh, ylow, yhigh, Nx, Ny)
    tri_points, tri_cells = createTriGrid(xlow, xhigh, ylow, yhigh, Nx, Ny)
    showGrid(tri_points, tri_cells)
    showGrid(rect_points, rect_cells)


if __name__ == '__main__':
    main()
