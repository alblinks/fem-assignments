import numpy as np

# points - identical for rectangular and triangular grid
points = np.array([[0.0,0.0],[0.5,0.0],[1.0,0.0],[0.0,0.5],[0.5,0.5],[1.0,0.5],[0.0,1.0],[0.5,1.0],[1.0,1.0]])

# cells of a rectangular grid
cellsRect = np.array([[0,1,3,4],[1,2,4,5],[3,4,6,7],[4,5,7,8]])

# cells of a triangular grid
cellsTri = np.array([[0,1,3],[4,3,1],[1,2,4],[5,4,2],[3,4,6],[7,6,4],[4,5,7],[8,7,5]])

def evalReferenceMap(points, cells, xHat):
    if type(xHat) is str:
        if xHat is "a0": xHat = np.array([0.0, 0.0])
        elif xHat is "a1": xHat = np.array([1.0, 0.0])
        elif xHat is "a2": xHat = np.array([0.0, 1.0])
        else:
            raise ValueError("Not a valid point on reference map.")
    elif type(xHat) is list:
        xHat = np.array(xHat)
    else:
        assert type(xHat) is np.ndarray, "xHat needs to be either point name (a0, a1, a2) or of type np.ndarray."

    trafoX = np.empty([np.shape(cells)[0], 2])
    for i in range(np.shape(cells)[0]):
        a0 = points[cells[i][0]]
        a1 = points[cells[i][1]]
        a2 = points[cells[i][2]]
        trafoX[i, :] = np.array([a1 - a0, a2 - a0]) @ xHat + a0
    return trafoX

t = evalReferenceMap(points, cellsRect, [2,0])
print(t)


def computeTrafoInformation(points, cells):
    trafoDet = np.empty(np.shape(cells)[0])
    invJac = np.empty([np.shape(cells)[0], 2, 2])
    for i in range(np.shape(cells)[0]):
        a0 = points[cells[i][0]]
        a1 = points[cells[i][1]]
        a2 = points[cells[i][2]]
        submatrix = np.array([a1-a0, a2-a0])
        trafoDet[i] = submatrix[0, 0] * submatrix[1, 1] - (submatrix[0, 1] * submatrix[1, 0])
        invJac[i] = np.linalg.inv(submatrix)
        # Check whether I'm stupid
        assert np.allclose(trafoDet[i], np.linalg.det(submatrix))
    assert np.shape(invJac) == (np.shape(cells)[0], 2, 2)
    return trafoDet, invJac

print(computeTrafoInformation(points, cellsTri))
