"""
	Objects represent a grid to be used e.g. with a Finite Element Method. 
	Datastructure:
		- points: all grid points; dimension Np x 2
		- cells: indices of nodes that form the vertices of a cells; dimension Nc x 3/4 (second axis depends if it's tri / rect)
	Constructor:
		- domain: Omega = [xlow,xhigh] x [ylow,yhigh]
		- divisions: (Nx,Ny) - number of grid points in the corresponding axis
		- tri: boolean value - 0 means rectangular grid, 1 means triangular
"""

import numpy as np
import matplotlib.pyplot as plt

class Grid:
    def __init__(self,xlow,xhigh,ylow,yhigh,Nx,Ny,tri):
            # Set properties of the class Grid
            self.xlow = xlow
            self.ylow = ylow
            self.xhigh = xhigh
            self.yhigh = yhigh
            self.Nx = Nx
            self.Ny = Ny

            if tri:
                self.points,self.cells = self.__createTriGrid__()
            else:
                self.points,self.cells = self.__createRectGrid__()
            
            self.dets,self.invJac = self.__computeTrafoInformation__()

    def __createRectGrid__(self):
            # vectors for division
            x = np.linspace(self.xlow,self.xhigh,self.Nx)
            y = np.linspace(self.ylow,self.yhigh,self.Ny)

            # number of cells in each dir is Nx-1 or Ny-1
            nc_x = self.Nx-1
            nc_y = self.Ny-1
    
            # meshgrid -> #nodes x 2
            xx,yy = np.meshgrid(x,y)
            points = np.stack((xx,yy), axis=2)
            points = points.reshape(self.Nx*self.Ny,2)

            # rectangles -> first one explicitely, then column and row shift
            cells = np.array([0, 1, self.Nx, self.Nx +1])  
            cells = np.tile(cells, (nc_x*nc_y,1))
        
            # column shift
            shift = np.arange(0,nc_x*nc_y).reshape(nc_x*nc_y,1)
            shift = np.repeat(shift,4,axis=1)
            cells = cells + shift
        
            # row shift
            shift = np.arange(0,nc_y).reshape(nc_y,1)
            shift = np.repeat(np.repeat(shift,nc_x,axis=0),4,axis=1)
            cells = cells + shift

            return points,cells     

    def __createTriGrid__(self):
            # vectors for division
            x = np.linspace(self.xlow,self.xhigh,self.Nx)
            y = np.linspace(self.ylow,self.yhigh,self.Ny)
        
            # number of retangles -> careful: this is not the number of cells
            nr_x = self.Nx-1
            nr_y = self.Ny-1
        
            # meshgrid -> #nodes x 2
            xx,yy = np.meshgrid(x,y)
            points = np.stack((xx,yy), axis=2)
            points = points.reshape(self.Nx*self.Ny,2)

            # triangles -> first 2 explicitely, then column and row shift
            cells = np.array([[0, 1, self.Nx],[self.Nx+1,self.Nx,1]])
            cells = np.tile(cells, (nr_x*nr_y,1))

            shift = np.arange(0,nr_x*nr_y).reshape(nr_x*nr_y,1)
            shift = np.repeat(np.repeat(shift,2,axis=0),3,axis=1)
            cells = cells + shift

            shift = np.arange(0,nr_y).reshape(nr_y,1)
            shift = np.repeat(np.repeat(shift,2*nr_x,axis=0),3,axis=1)
            cells = cells + shift

            return points,cells   
        
    def __computeTrafoInformation__(self):
            verts = self.points[self.cells,0:2]
            verts = np.swapaxes(verts,2,1)
       
            # det(A) = ad-bc
            trafoDet = np.multiply(verts[:,0,1]-verts[:,0,0],verts[:,1,2]-verts[:,1,0]) - np.multiply(verts[:,0,2]-verts[:,0,0],verts[:,1,1]-verts[:,1,0])
    
            # adjoint matrices ----> TRANSPOSED
            firstCol = np.stack((verts[:,1,2]-verts[:,1,0],verts[:,0,0]-verts[:,0,2]),axis=1)
            secondCol = np.stack((verts[:,1,0]-verts[:,1,1],verts[:,0,1]-verts[:,0,0]),axis=1)
            adjMat = np.stack((firstCol,secondCol),axis=2)
    
            # inverse 
            invJac = np.divide(adjMat,trafoDet[:,np.newaxis,np.newaxis])
    
            return trafoDet,invJac

    def evalReferenceMap(self, xHat):
            """
                INPUT: xHat has dimension N x 2 with each row being a point in the reference configuration
                OUTPUT: dimension Nc x N x 2 with a (2D) point for each cell of the grid and each row of xHat (input point)
            """
            # get vertices in format nEx2x3
            verts = self.points[self.cells,0:3]
            verts = np.swapaxes(verts,2,1)
    
            # define reference matrix and vector
            trafoVec = verts[:,:,0]
            trafoMat = np.stack((verts[:,:,1]-verts[:,:,0],verts[:,:,2]-verts[:,:,0]),axis=2)   

            # constant has to be repeated depending on the number of xHat
            trafoVec = np.tile(trafoVec,(xHat.shape[0],1,1))
            trafoVec = np.rollaxis(trafoVec,1,0)

            return np.dot(trafoMat,xHat) + trafoVec 

    def show(self):
            plt.figure()
            # limits 
            plt.gca().set_xlim([self.xlow, self.xhigh])
            plt.gca().set_ylim([self.ylow, self.yhigh])

            for i in range(self.cells.shape[0]):
                verts = self.points[self.cells[i],:]
                # switch order for rectangles 
                if verts.shape[0] == 4:
                    verts = verts[[0,1,3,2],:]

                cell = plt.Polygon(verts,fill=0,color='xkcd:blue')
                plt.gca().add_patch(cell)

            plt.show()
